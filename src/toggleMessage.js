import React, { Component } from 'react';
import {validation, ValidContext} from './context-validation';

class ToggleMessage  extends Component {

    render(){
        let context = this.context;
        console.log(this.context);
        return (
            //<ValidContext.Consumer>
                <div className="valid-message">
                    {
                        context === 'valid'? validation.valid.mess : validation.invalid.mess
                    }
                </div>
            //</ValidContext.Consumer>


        )
    }
}
ToggleMessage.contextType = ValidContext;
export default ToggleMessage;
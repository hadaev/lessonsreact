import React, { Component } from 'react';
import {ValidContext} from './context-validation';
import Message from './message';

class Form  extends Component {
    constructor(props){
        super(props);

        this.state = {
            email: '',
            password: '',
            formErrors: {email: '', password: ''},
            emailValid: false,
            passwordValid: false,
            formValid: false
        }
    }

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
            () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? 'valid' : 'invalid';
                break;
            case 'password':
                passwordValid = value.length >= 6;
                fieldValidationErrors.password = passwordValid ? 'valid' : 'invalid';
                break;
            default:
                break;
        }

        this.setState({
            formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid
          }, this.validateForm);
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid});
    }

    validContext(){
        if(this.state.formValid)
            return 'valid';
        else
            return 'invalid';
    }

    render(){
        return (
            <form className="form">
                <ValidContext.Provider value={this.validContext()}>
                    <Message/>
                </ValidContext.Provider>
                <div className={`form-group ${this.state.formErrors.email}`}>
                    <label htmlFor="email">Email address</label>
                    <input type="email" required className="form-control" name="email"
                           placeholder="Email"
                           value={this.state.email}
                           onChange={this.handleUserInput}  />
                </div>
                <div className={`form-group ${this.state.formErrors.password}`}>
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" name="password"
                           placeholder="Password"
                           value={this.state.password}
                           onChange={this.handleUserInput}  />
                </div>
                <button type="submit" className="btn btn-primary" disabled={!this.state.formValid}>Sign up</button>
            </form>
        )
    }
}
Form.contextType = ValidContext;
export default Form;
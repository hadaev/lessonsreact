import React, { Component } from 'react';
let mess1 = <div className="valid"><h2>Поля заполнены правильно</h2></div>;
let mess2 = <div className="invalid"><h2>Заполните поля!</h2></div>;

export const validation = {
    valid: {mess: mess1},
    invalid: {mess: mess2}
};

export  const ValidContext = React.createContext( validation.valid);
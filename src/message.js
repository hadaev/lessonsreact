import React, { Component } from 'react';
import ToggleMessage from './toggleMessage';

class Message  extends Component {
    render(){
        return (
            <div className="message">
                <ToggleMessage/>
            </div>
        )
    }
}
export default Message;